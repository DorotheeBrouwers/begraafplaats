package com.example.doortje.begraafplaatsen.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.doortje.begraafplaatsen.InputOutput.HttpGetAsyncTask;
import com.example.doortje.begraafplaatsen.InputOutput.IAsyncCallback;
import com.example.doortje.begraafplaatsen.InputOutput.JSONContract;
import com.example.doortje.begraafplaatsen.Models.LijstOverledenenAdapter;
import com.example.doortje.begraafplaatsen.Models.Overledene;
import com.example.doortje.begraafplaatsen.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_ALL_OVERLEDENEN;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_BURIAL_PLACE_TYPE;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_DATE_LAST_MODIFICATION;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_DATE_OF_BIRTH;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_DATE_OF_BURIAL;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_DATE_OF_DEATH;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_FIRST_NAME;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_ID;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_IDENTIFICATION_NUMBER;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_LAST_NAME;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_LOCATION_NUMBER;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_NAME;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_NATURE_ENTOMBMENT;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_PLACE_OF_BIRTH;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_REGISTRATION_NUMBER;

/**
 * http://www.javacodegeeks.com/2013/09/android-listview-with-adapter-example.html
 */
public class MainActivity extends Activity {


    /**
     * variabelen declareren
     */
    private ListView lijstOverledenen;
    private IAsyncCallback<String> resultListener;
    private ArrayList<Overledene> overledeneArrayList;
    //private ArrayList<Begraafplaats> begraafplaatsArrayList;
    private EditText achternaamET, voornaamET;
    private Button zoekBTN;
    private Overledene geselecteerdeOverledene;
    //private Kerkhof naamKerkhof;
    //private Begraafplaats bgf;


    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.searchscreen_activity_main);

        achternaamET = (EditText) findViewById(R.id.et_invulveld_achternaam_searchscreen);
        achternaamET.getText();

        voornaamET = (EditText) findViewById(R.id.et_invulveld_voornaam_searchscreen);
        voornaamET.getText();

        zoekBTN = (Button) findViewById(R.id.btn_zoek_searchscreen);
        zoekBTN.setOnClickListener(new SearchScreenHandler());

        lijstOverledenen = (ListView) findViewById(R.id.lv_zoekresultaten_searchscreen);

        //Getting JSON from URL
        //String jsonObject = HttpInputOutput.getHttpGetRequestContent(JSONContract.url);

        // er wordt een callback aangeroepen
        resultListener = new IAsyncCallback<String>() {
            @Override
            public void onOperationCompleted(String result) {
                Log.d("JSON binnenhalen", "succes" + result);

                //Getting JSON Array

                try {
                    if (result.contains(TAG_ALL_OVERLEDENEN)) {
                        //try to parse the string to a JSON object
                        JSONObject resultObj = new JSONObject(result);
                        Object obj = resultObj.get(TAG_ALL_OVERLEDENEN);
                        //nieuwe lege arraylist aanmaken
                        overledeneArrayList = new ArrayList<>();

                        //test die true weergeeft als obj van het type JSONArray is
                        if (obj instanceof JSONArray) {
                            JSONArray overledenenArray = (JSONArray) obj;

                            //loopen over de volledige overledenenArray en telkens 1 plaats opschuiven.
                            for (int i = 0; i < overledenenArray.length(); i++) {
                                try {
                                    resultObj = overledenenArray.getJSONObject(i);
                                    overledeneArrayList.add(convertJsonObjectToOverledene(resultObj));

                                    // catch
                                } catch (JSONException e) {
                                    e.printStackTrace();

                                }

                                Log.d("Parsen naar JSONObject", "SUCCES");
                            }
                        } else {
                            Overledene overledene = convertJsonObjectToOverledene(resultObj.getJSONObject(TAG_ALL_OVERLEDENEN));
                            if (overledene != null) {
                                overledeneArrayList.add(overledene);
                            }
                        }
                        //methode om lijst te vullen via de adapter (zie methode onder)
                        fillOverledenenLijst();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        lijstOverledenen.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                geselecteerdeOverledene = overledeneArrayList.get(position);
                //bgf = begraafplaatsArrayList.get(position);
                Intent goToMapIntent = new Intent(getApplicationContext(), MapActivity.class);
                goToMapIntent.putExtra("Achternaam", geselecteerdeOverledene.getLast_name());
                goToMapIntent.putExtra("Voornaam", geselecteerdeOverledene.getFirst_name());
                goToMapIntent.putExtra("Geboortejaar", geselecteerdeOverledene.getDate_of_birth());
                goToMapIntent.putExtra("Sterftejaar", geselecteerdeOverledene.getDate_of_death());
                //goToMapIntent.putExtra("Park",bgf.getPark());
                //goToMapIntent.putExtra("Rij", bgf.getRow());
                // goToMapIntent.putExtra("Begraafplaats =",naamKerkhof.getBgf_parks());
                startActivity(goToMapIntent);
                Log.d("onItemClick", "Lukt ");
            }
        });
        //  final HttpGetAsyncTask asyncTask = new HttpGetAsyncTask(resultListener, this);
        //  asyncTask.execute();
    }

    // omdat je dit stukje meermaals zou gebruikt hebben binnen deze code, maak je hier een aparte methode van,
    // zodat je maar 1 regel code moet gebruiken binnen andere stukken code. Indien je iets meermaals gebruikt,
    // maak je er een aparte methode voor/van.
    public Overledene convertJsonObjectToOverledene(JSONObject resultObj) {
        Overledene temp = null;
        //wordt aangemaakt omdat de ID in de "@attributes" staat.
        JSONObject attribute = null;
        try {
            attribute = resultObj.getJSONObject("@attributes");

            temp = new Overledene(attribute.getString(TAG_ID),
                    resultObj.getString(TAG_NAME),
                    resultObj.getString(TAG_IDENTIFICATION_NUMBER),
                    resultObj.getString(TAG_LAST_NAME),
                    resultObj.getString(TAG_FIRST_NAME),
                    resultObj.getString(TAG_DATE_OF_DEATH),
                    resultObj.getString(TAG_DATE_OF_BURIAL),
                    resultObj.getString(TAG_LOCATION_NUMBER),
                    resultObj.getString(TAG_DATE_LAST_MODIFICATION),
                    resultObj.getString(TAG_NATURE_ENTOMBMENT),
                    resultObj.getString(TAG_REGISTRATION_NUMBER),
                    resultObj.getString(TAG_BURIAL_PLACE_TYPE));

            if (resultObj.has(TAG_DATE_OF_BIRTH)) {
                temp.setDate_of_birth(resultObj.getString(TAG_DATE_OF_BIRTH));
            }

            if (resultObj.has(TAG_PLACE_OF_BIRTH)) {
                temp.setPlace_of_birth(resultObj.getString(TAG_PLACE_OF_BIRTH));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d("convert to Overledene", "OK" + resultObj);

        return temp;
    }

    /**
     *
     */
    private void fillOverledenenLijst() {

        //construct the data source
        // overledeneArrayList = new ArrayList<Overledene>();

        //Create the adapter to convert the array to views
        LijstOverledenenAdapter overledenenAdapter = new LijstOverledenenAdapter(this, overledeneArrayList);

        //Attach the adapter to the listview
        lijstOverledenen = (ListView) findViewById(R.id.lv_zoekresultaten_searchscreen);
        lijstOverledenen.setAdapter(overledenenAdapter);
    }

   /* private void fillBegraafplaatsLijst() {
        //create the adapter to the listview
        LijstBegraafplaatsenAdapter begraafplaatsenAdapter = new LijstBegraafplaatsenAdapter(this,begraafplaatsArrayList);

        //Attach the adapter to the listview
        lijstOverledenen.setAdapter(begraafplaatsenAdapter);
    }*/

    /**
     * eigen handler voor de ZoekBTN. Deze kijkt na of het textveld ingevuld staat.
     * Indien niet, geeft deze een error mee aan de gebruiker.
     * Indien wel, wordt er een nieuwe AsyncTask aangemaakt met nieuwe parameters (Zie verder bij HttpGetAsyncTask.java)
     */
    private class SearchScreenHandler implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            Log.d("SearchScreenHandler", "onClick");
            String achternaam = achternaamET.getText().toString();
            String voornaam = voornaamET.getText().toString();

            HttpGetAsyncTask newAsync = new HttpGetAsyncTask(resultListener, getApplicationContext());
            if (achternaam == null || achternaam.isEmpty()) {
                achternaamET.setError("Vul een achternaam in");
            } else { // er zit iets in achternaam
                if (voornaam == null || voornaam.isEmpty()) { // enkel achternaam
                    String fullurl = JSONContract.url + "?last_name=" + achternaam;
                    newAsync.execute(fullurl);
                    Log.d("SearchScreenHandler", "Plakt de naam achter de url" + fullurl);
                } else { // ook voornaam aanwezig
                    String compleeturl = JSONContract.url + "?last_name=" + achternaam + "&first_name=" + voornaam;
                    newAsync.execute(compleeturl);
                    Log.d("SearchScreenHandler", "Plakt achternaam en voornaam aan de url" + compleeturl);

                }
            }
        }
    }

    /**
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
