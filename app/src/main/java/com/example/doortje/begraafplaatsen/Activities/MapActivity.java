package com.example.doortje.begraafplaatsen.Activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.example.doortje.begraafplaatsen.InputOutput.HttpGetAsyncTask;
import com.example.doortje.begraafplaatsen.InputOutput.IAsyncCallback;
import com.example.doortje.begraafplaatsen.Models.Begraafplaats;
import com.example.doortje.begraafplaatsen.Models.Kerkhof;
import com.example.doortje.begraafplaatsen.R;
import com.example.doortje.begraafplaatsen.Utils.Lambert72toWGS84;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolygonOptions;
import com.orhanobut.logger.Logger;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_ALL_BURIAL_PLACES;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_ALL_CEMETERIES;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_ALL_OVERLEDENEN;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_BURIAL_PLACE_BGF_CONCESSION;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_BURIAL_PLACE_BGF_DECEASEDS;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_BURIAL_PLACE_DATE_LAST_MODIFICATION;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_BURIAL_PLACE_ID;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_BURIAL_PLACE_IDENTIFICATION_NUMBER;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_BURIAL_PLACE_PARK;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_BURIAL_PLACE_ROW;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_BURIAL_PLACE_TYPE;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_CEMETERY_BGF_DECEASED;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_CEMETERY_BGF_PARKS;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_CEMETERY_DATE_OF_LAST_MODIFICATION;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_CEMETERY_ID;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_CEMETERY_NAME;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.TAG_CEMETERY_POSLIST;
import static com.example.doortje.begraafplaatsen.InputOutput.JSONContract.url;

/**
 * Created by doortje on 8/07/15.
 */
public class MapActivity extends Activity implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener {
    /**
     * variabelen declareren
     */

    private static final String TAG = "MapActivity";

    private TextView achternaamTV, voornaamTV, geboortejaarTV, sterftejaarTV, parknummerTV, begraafplaatsTV;
    private IAsyncCallback<String> resultListener;
    private ArrayList<Kerkhof> kerkhofArrayList;
    private ArrayList<Begraafplaats> begraafplaatsArrayList;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mapscreen_activity);

        MapFragment mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.kaartfragment);
        mapFragment.getMapAsync(this);

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }
        String achterNaam = extras.getString("Achternaam");
        String voorNaam = extras.getString("Voornaam");
        String geboorteJaar = extras.getString("Geboortejaar");
        String sterfteJaar = extras.getString("Sterftejaar");
        //String parkNummer = extras.getString("Parknummer");
        //String parkRij = extras.getString("Parkrij");
        //String begraafPlaats = extras.getString("Begraafplaats");


        achternaamTV = (TextView) findViewById(R.id.tv_achternaam_mapscreen);
        achternaamTV.setText(achterNaam);
        voornaamTV = (TextView) findViewById(R.id.tv_voornaam_mapscreen);
        voornaamTV.setText(voorNaam);
        geboortejaarTV = (TextView) findViewById(R.id.tv_geboortejaar_mapscreen);
        geboortejaarTV.setText(geboorteJaar);
        sterftejaarTV = (TextView) findViewById(R.id.tv_sterftejaar_mapscreen);
        sterftejaarTV.setText(sterfteJaar);
        //parknummerTV = (TextView) findViewById(R.id.tv_parknummer_mapscreen);
        //parknummerTV.setText();
        //parknummerTV.setText(parkRij);
        //begraafplaatsTV = (TextView) findViewById(R.id.tv_begraafplaats_mapscreen);
        //begraafplaatsTV.setText(begraafPlaats);
        Log.d("Invullen van TV's", "Set!");

        //Getting JSON from URL
        HttpGetAsyncTask newAsync = new HttpGetAsyncTask(resultListener, getApplicationContext());
        newAsync.execute(url);

        // er wordt een callback aangeroepen
        resultListener = new IAsyncCallback<String>() {
            @Override
            public void onOperationCompleted(String result) throws JSONException {
                Log.d("JSON binnenhalen", "succes" + result);

                //Getting JSON Array
                if (result.contains(TAG_ALL_CEMETERIES)) {
                    //try to parse the string to a JSON object
                    try {
                        JSONObject mapcemeteryresultObj = new JSONObject(result);
                        Object mapObj = mapcemeteryresultObj.get(TAG_ALL_CEMETERIES);
                        //nieuwe lege arraylist aanmaken
                        kerkhofArrayList = new ArrayList<>();
                        Log.d("Arraylist", "check");

                        //test die true weergeeft als obj van het type JSONArray is
                        if (mapObj instanceof JSONArray) {
                            JSONArray kerkhofArray = (JSONArray) mapObj;
                            Log.d("Elk object in Array?", "Aangemaakt" + kerkhofArray);

                            //loopen over de volledig kerkhofArray
                            for (int i = 0; i < kerkhofArray.length(); i++) {
                                try {
                                    mapcemeteryresultObj = kerkhofArray.getJSONObject(i);
                                    kerkhofArrayList.add(convertJsonObjectToKerkhof(mapcemeteryresultObj));
                                    Log.d("For loop", "Werkt");

                                    //catch
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                Log.d("Parsen naar JSONObject", "MAPRESULTS" + kerkhofArrayList);
                            }
                        } else {

                            Kerkhof kerkhof = convertJsonObjectToKerkhof(mapcemeteryresultObj.getJSONObject(TAG_ALL_CEMETERIES));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

                if (result.contains(TAG_ALL_BURIAL_PLACES)) {
                    //try to parse the string to a JSON object
                    try {
                        JSONObject mapburialplaceresultObj = new JSONObject(result);
                        Object bgfObj = mapburialplaceresultObj.get(TAG_ALL_BURIAL_PLACES);
                        //nieuwe lege arraylist aanmaken
                        begraafplaatsArrayList = new ArrayList<>();
                        Log.d("BegraafplaatsArrayList", "check");

                        //test die true weergeeft als obj van het type JSONArray is
                        if (bgfObj instanceof JSONArray) {
                            JSONArray begraafplaatsArray = (JSONArray) bgfObj;
                            Log.d("Elk object in Array?", "Aangemaakt" + begraafplaatsArray);


                            //loopen over de volledige begraafplaatsArray
                            for (int i = 0; i < begraafplaatsArray.length(); i++) {
                                try {
                                    mapburialplaceresultObj = begraafplaatsArray.getJSONObject(i);
                                    begraafplaatsArrayList.add(convertJsonObjectToBegraafplaats(mapburialplaceresultObj));
                                    Log.d("For loop", "Werkt!");
                                    //catch
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                                Log.d("Parsen naar JSONObject", "BurialPlaceresults" + begraafplaatsArrayList);
                            }
                        } else {
                            Begraafplaats begraafplaats = convertJsonObjectToBegraafplaats(mapburialplaceresultObj.getJSONObject(TAG_ALL_OVERLEDENEN));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }


            public Kerkhof convertJsonObjectToKerkhof(JSONObject mapcemeteryresultObj) {
                Kerkhof temp = null;
                //wordt aangemaakt omdat de ID in de "@attributes" staat.
                JSONObject mapAattribute = null;
                JSONObject cemeteryObj = null;
                try {

                    mapAattribute = mapcemeteryresultObj.getJSONObject("@attributes");
                    cemeteryObj = mapcemeteryresultObj.getJSONObject("coverage")
                            .getJSONObject("Polygon")
                            .getJSONObject("exterior")
                            .getJSONObject("LinearRing");

                    temp = new Kerkhof(mapAattribute.getString(TAG_CEMETERY_ID),
                            mapcemeteryresultObj.getString(TAG_CEMETERY_NAME),
                            cemeteryObj.getString(TAG_CEMETERY_POSLIST),
                            mapcemeteryresultObj.getString(TAG_CEMETERY_DATE_OF_LAST_MODIFICATION),
                            mapcemeteryresultObj.getString(TAG_CEMETERY_BGF_PARKS),
                            mapcemeteryresultObj.getString(TAG_CEMETERY_BGF_DECEASED));

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d("Convertion", "ok");

                return temp;
            }

            public Begraafplaats convertJsonObjectToBegraafplaats(JSONObject mapburialplaceresultObj) {

                Begraafplaats temp = null;
                // wordt aangemaakt omdat de ID in de "@attributes" staat.
                JSONObject bgfAttribute = null;
                JSONObject begraafplaatsObj = null;
                try {
                    bgfAttribute = mapburialplaceresultObj.getJSONObject("@attributes");

                    temp = new Begraafplaats(bgfAttribute.getString(TAG_BURIAL_PLACE_ID),
                            mapburialplaceresultObj.getString(TAG_BURIAL_PLACE_IDENTIFICATION_NUMBER),
                            mapburialplaceresultObj.getString(TAG_BURIAL_PLACE_TYPE),
                            mapburialplaceresultObj.getString(TAG_BURIAL_PLACE_BGF_DECEASEDS),
                            mapburialplaceresultObj.getString(TAG_BURIAL_PLACE_BGF_CONCESSION),
                            mapburialplaceresultObj.getString(TAG_BURIAL_PLACE_DATE_LAST_MODIFICATION),
                            mapburialplaceresultObj.getString(TAG_BURIAL_PLACE_ROW),
                            mapburialplaceresultObj.getString(TAG_BURIAL_PLACE_PARK));

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                Log.d("Convertion Bgf", "OK");

                return temp;

            }

        };

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        Toast.makeText(getApplicationContext(), " Kaart is geladen", Toast.LENGTH_SHORT).show();
        mMap = googleMap;
        mMap.setOnMapClickListener(this);
        mMap.setOnMarkerClickListener(this);
        mMap.setOnInfoWindowClickListener(this);

        LatLng kerkhofGenkCentrumCoord = new LatLng(50.956101, 5.504555);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(kerkhofGenkCentrumCoord, 17);
        mMap.animateCamera(cameraUpdate);

        Stringsplit ss = new Stringsplit();
        ss.map("2.29949667E8 1.83431246E8 2.29883403E8 1.83522478E8 2.2998948E8 1.83585787E8 2.29970219E8 1.8361499E8 2.29916687E8 1.83696152E8 2.29902517E8 1.83718026E8 2.29606424E8 1.8350506E8 2.29745165E8 1.83312164E8 2.29746054E8 1.83310935E8 2.29810395E8 1.83342025E8 2.29865998E8 1.83377403E8 2.29926955E8 1.83416485E8 2.29944297E8 1.83427755E8 2.29945282E8 1.83428398E8 2.29945395E8 1.83428472E8 2.29946469E8 1.83429164E8 2.29947871E8 1.83430077E8 2.29948365E8 1.83430399E8 2.29948879E8 1.83430733E8 2.29949075E8 1.8343086E8 2.29949423E8 1.83431087E8 2.29949634E8 1.83431224E8 2.29949664E8 1.83431244E8 2.29949667E8 1.83431246E8");


    }

    @Override
    public void onInfoWindowClick(Marker marker) {

    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    /*
    Java String class defines following methods to split Java String object.
    String[] split( String regularExpression )
    Splits the string according to given regular expression.
    String[] split( String reularExpression, int limit )
    Splits the string according to given regular expression. The number of resultant
    substrings by splitting the string is controlled by limit argument.
    */
    public class Stringsplit {

        public void map(String arg) {

            String posList = arg; //TAG_CEMETERY_POSLIST.toString();
            //posList = posList.replace(".",""); // Niet nodig - gevaarlijk
            //posList = posList.replace("E8","");// Niet nodig - gevaarlijk

            Log.d("poslist", "ok!" + posList);

            String[] parts = posList.split(" ");
            // de volledige String is nu in aparte String gesplitst
            Log.d("StringSplit", "Gelukt!" + posList);

            ArrayList<LatLng> polygonLatLngs = new ArrayList<>();

            for (int i = 0; i < parts.length; i += 2) {
                String positionX = parts[i];
                String positionY = parts[i + 1];

                // string bevat coördinaten in milimeter - we hebben meters nodig
                double positionXDouble = Double.parseDouble(positionX) / 1000;
                double positionYDouble = Double.parseDouble(positionY) / 1000;

                Log.d("positionXDouble", "ok? " + positionXDouble);
                Log.d("positionYDouble", "ok? " + positionYDouble);

                LatLng latLongCoordinates = Lambert72toWGS84.lambert72toWGS84New(positionXDouble,positionYDouble);
                polygonLatLngs.add(latLongCoordinates);
                Log.d("convertedArrayList", "lat x long " + latLongCoordinates.longitude + "' x " + latLongCoordinates.latitude);

                Logger.d(TAG);
                Logger.d(latLongCoordinates.longitude + "," + latLongCoordinates.latitude);
            }

            PolygonOptions kerkhofPolygonOptions = new PolygonOptions()
                    .addAll(polygonLatLngs)
                    .strokeColor(R.color.dark_green)
                    .fillColor(R.color.light_green)
                    .visible(true);

            List<LatLng> points = kerkhofPolygonOptions.getPoints();
            if (!points.isEmpty()) {
                //Get back the mutable Polygon
                Polygon kerkhofPolygon = mMap.addPolygon(kerkhofPolygonOptions);

                //TODO je doet niks met kerkhofPolygon, denk ik

                Log.d("Poly lines", "Succesfully added on map");

            }
        }
    }
}











