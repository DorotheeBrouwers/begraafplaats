package com.example.doortje.begraafplaatsen.InputOutput;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONException;

import java.util.Arrays;

/**
 * @author doortje
 * hnjklnjklnj
 */
public class HttpGetAsyncTask extends AsyncTask<String, Void, String> {

    final private IAsyncCallback<String> resultListener;
    private boolean connected;
    private HttpInputOutput httpInputOutput;

    /**
     *
     * @param resultListener
     * @param context
     */
    public HttpGetAsyncTask(IAsyncCallback<String> resultListener, Context context) {
        this.resultListener = resultListener;
        ConnectivityManager connectivityManager =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED)
            connected = true;
        else
            connected = false;
        Log.d("Connection", "succes" + resultListener);
    }

    //dit is alles wat er gebeurt op de achtergrond, onderliggend van de app

    /**
     *
     * @param params
     * @return
     * (String...params) --> ("a","b","c")
     * params [...] --> ({"a","b","c"})
     */
    @Override
    protected String doInBackground(String... params) {
        //Log.d kan ook op deze manier uitgevoerd worden.
        Log.d("doInBackground", "params: " + Arrays.toString(params));

        String url = params[0];
        if (connected) {
            httpInputOutput = new HttpInputOutput();
            //de parameter was hier eerst JSONContract.url, maar deze is in de SearchScreenHandler omgezet naar url.
            return HttpInputOutput.getHttpGetRequestContent(url);
        }
        return null;
    }


    //methode voor dat er iets wordt uitgevoerd

    /**
     *
     * @param result
     */
    @Override
    protected void onPostExecute(String result) {
        if (this.resultListener != null) {
            try {
                this.resultListener.onOperationCompleted(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d("onPostExecute", "succes");
        }
    }

}
