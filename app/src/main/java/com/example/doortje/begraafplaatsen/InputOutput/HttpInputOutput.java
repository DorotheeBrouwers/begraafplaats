package com.example.doortje.begraafplaatsen.InputOutput;

import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

/**
 * Created by doortje on 3/07/15.
 */

public class HttpInputOutput {

    static JSONObject jsonObject = null;
    static String jsonString = "";

    //constructor
    public HttpInputOutput() {
    }

    /**
     * Returns the content of the response to a http request.
     *
     * @param url
     * @return
     */

    public static String getHttpGetRequestContent(String url) {
        BufferedReader reader = null;
        StringBuilder builder = new StringBuilder();
        InputStream inputStream = null;

        //making HTTP request
        try {
            URL urlDataOverledene = new URL(url);
            URLConnection urlConnection = (URLConnection) urlDataOverledene.openConnection();
            urlConnection.connect();
            inputStream = new BufferedInputStream(urlConnection.getInputStream());

            /* de datastream wordt van het internet binnengetrokken in 1 lange String, deze wordt volledig gelezen door de reader.
            normaal lijn per lijn.
            na het lezen wordt de builder hier aan vastgekoppeld.*/

            reader = new BufferedReader(new InputStreamReader(inputStream));
            String jsonString = reader.readLine();
            while (jsonString != null) {
                builder.append(jsonString);
                jsonString = reader.readLine();
                //is er een URLconnectie? Werkt de reader?
            }
            Log.d("URLConnectie", "done " + builder.toString());

            inputStream.close();
            jsonString = builder.toString();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException iae) {
            iae.printStackTrace();
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (IOException ioe) {
                ioe.printStackTrace();
            }
        }

        return builder.toString();
    }
}
