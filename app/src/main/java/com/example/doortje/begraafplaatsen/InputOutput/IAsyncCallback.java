package com.example.doortje.begraafplaatsen.InputOutput;

/**
 * Created by doortje on 3/07/15.
 */

import org.json.JSONException;

/**
 *
 * @param <T>
 */
public interface IAsyncCallback <T> {
    void onOperationCompleted (T result) throws JSONException;
}
