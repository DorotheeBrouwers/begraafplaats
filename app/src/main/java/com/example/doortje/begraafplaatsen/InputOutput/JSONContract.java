package com.example.doortje.begraafplaatsen.InputOutput;

/**
 * Created by doortje on 3/07/15.
 */
public class JSONContract {

    /**
     * URL to get JSON Array
     */
    public static String url = "http://data.pxl.be/genk/begraafplaatsen/deceased";
    /**
     * GET requests
     * JSON Node names
     */
    public static final String TAG_ALL_OVERLEDENEN = "bgf.bgf_deceased";

    public static final String TAG_ID = "id";
    public static final String TAG_NAME = "name";
    public static final String TAG_IDENTIFICATION_NUMBER = "identification_number";
    public static final String TAG_LAST_NAME ="last_name";
    public static final String TAG_FIRST_NAME = "first_name";
    public static final String TAG_PLACE_OF_BIRTH = "place_of_birth";
    public static final String TAG_DATE_OF_DEATH = "date_of_death";
    public static final String TAG_DATE_OF_BURIAL = "date_of_burial";
    public static final String TAG_DATE_OF_BIRTH = "date_of_birth";
    public static final String TAG_LOCATION_NUMBER = "location_number";
    public static final String TAG_DATE_LAST_MODIFICATION = "date_last_modification";
    public static final String TAG_NATURE_ENTOMBMENT = "nature_entombment";
    public static final String TAG_REGISTRATION_NUMBER ="registration_number";
    public static final String TAG_BURIAL_PLACE_TYPE ="burial_place_type";

    public static final String TAG_ALL_CEMETERIES = "bgf.bgf_cemetery";

    public static final String TAG_CEMETERY_ID = "id";
    public static final String TAG_CEMETERY_NAME = "name";
    public static final String POINT_F_LIST = "Polygon";
    public static final String TAG_CEMETERY_POSLIST = "posList";
    public static final String TAG_CEMETERY_DATE_OF_LAST_MODIFICATION = "date_last_modification";
    public static final String TAG_CEMETERY_BGF_PARKS = "bgf_parks";
    public static final String TAG_CEMETERY_BGF_DECEASED = "bgf_deceaseds";

    public static final String TAG_ALL_BURIAL_PLACES = "bgf.bgf_burial_place";

    public static final String TAG_BURIAL_PLACE_ID ="id";
    public static final String TAG_BURIAL_PLACE_IDENTIFICATION_NUMBER = "identification_number";
    public static final String TAG_BURIAL_PLACE_BP_TYPE = "burial_place_type";
    public static final String TAG_BURIAL_PLACE_BGF_DECEASEDS = "bgf_deceaseds";
    public static final String TAG_BURIAL_PLACE_BGF_CONCESSION = "bgf_concession";
    public static final String TAG_BURIAL_PLACE_DATE_LAST_MODIFICATION = "date_last_modification";
    public static final String TAG_BURIAL_PLACE_ROW ="row";
    public static final String TAG_BURIAL_PLACE_PARK = "park";



}
