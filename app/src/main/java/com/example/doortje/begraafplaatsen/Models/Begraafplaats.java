package com.example.doortje.begraafplaatsen.Models;

/**
 * Created by doortje on 13/07/15.
 */
public class Begraafplaats {

    private String id;
    private String identification_number;
    private String burial_place_type;
    private String bgf_deceaseds;
    private String bgf_concession;
    private String date_last_modification;
    private String row;
    private String park;

    public Begraafplaats (){}

    public Begraafplaats(String id, String identification_number, String burial_place_type, String bgf_deceaseds, String bgf_concession, String date_last_modification, String row, String park) {
        this.id = id;
        this.identification_number = identification_number;
        this.burial_place_type = burial_place_type;
        this.bgf_deceaseds = bgf_deceaseds;
        this.bgf_concession = bgf_concession;
        this.date_last_modification = date_last_modification;
        this.row = row;
        this.park = park;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdentification_number() {
        return identification_number;
    }

    public void setIdentification_number(String identification_number) {
        this.identification_number = identification_number;
    }

    public String getBurial_place_type() {
        return burial_place_type;
    }

    public void setBurial_place_type(String burial_place_type) {
        this.burial_place_type = burial_place_type;
    }

    public String getBgf_deceaseds() {
        return bgf_deceaseds;
    }

    public void setBgf_deceaseds(String bgf_deceaseds) {
        this.bgf_deceaseds = bgf_deceaseds;
    }

    public String getBgf_concession() {
        return bgf_concession;
    }

    public void setBgf_concession(String bgf_concession) {
        this.bgf_concession = bgf_concession;
    }

    public String getDate_last_modification() {
        return date_last_modification;
    }

    public void setDate_last_modification(String date_last_modification) {
        this.date_last_modification = date_last_modification;
    }

    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    public String getPark() {
        return park;
    }

    public void setPark(String park) {
        this.park = park;
    }

    @Override
    public String toString() {
        return "Begraafplaats{" +
                "id='" + id + '\'' +
                ", identification_number='" + identification_number + '\'' +
                ", burial_place_type='" + burial_place_type + '\'' +
                ", bgf_deceaseds='" + bgf_deceaseds + '\'' +
                ", bgf_concession='" + bgf_concession + '\'' +
                ", date_last_modification='" + date_last_modification + '\'' +
                ", row='" + row + '\'' +
                ", park='" + park + '\'' +
                '}';
    }
}
