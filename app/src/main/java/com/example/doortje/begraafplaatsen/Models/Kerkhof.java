package com.example.doortje.begraafplaatsen.Models;

import android.graphics.PointF;

import java.util.List;

/**
 * Created by doortje on 8/07/15.
 */
public class Kerkhof {

    private String id;
    private String name;
    private String Polygon;
    private String date_last_modification;
    private String bgf_parks;
    private String bgf_deceaseds;

    public Kerkhof() {
    }

    public Kerkhof(String id, String name, String polygon, String date_last_modification, String bgf_parks, String bgf_deceaseds) {
        this.id = id;
        this.name = name;
        Polygon = polygon;
        this.date_last_modification = date_last_modification;
        this.bgf_parks = bgf_parks;
        this.bgf_deceaseds = bgf_deceaseds;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<PointF> getPolygon() {

        return null;
    }



    public String getDate_last_modification() {
        return date_last_modification;
    }

    public void setDate_last_modification(String date_last_modification) {
        this.date_last_modification = date_last_modification;
    }

    public String getBgf_parks() {
        return bgf_parks;
    }

    public void setBgf_parks(String bgf_parks) {
        this.bgf_parks = bgf_parks;
    }

    public String getBgf_deceaseds() {
        return bgf_deceaseds;
    }

    public void setBgf_deceaseds(String bgf_deceaseds) {
        this.bgf_deceaseds = bgf_deceaseds;
    }

    @Override
    public String toString() {
        return "Kerkhof{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", Polygon=" + Polygon +
                ", date_last_modification='" + date_last_modification + '\'' +
                ", bgf_parks='" + bgf_parks + '\'' +
                ", bgf_deceaseds='" + bgf_deceaseds + '\'' +
                '}';
    }
}
