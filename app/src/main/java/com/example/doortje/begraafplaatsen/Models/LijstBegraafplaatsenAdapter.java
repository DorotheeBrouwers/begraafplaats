package com.example.doortje.begraafplaatsen.Models;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.doortje.begraafplaatsen.R;

import java.util.ArrayList;

/**
 * Created by doortje on 13/07/15.
 */
public class LijstBegraafplaatsenAdapter extends ArrayAdapter <Begraafplaats> {

    //private ArrayList <Begraafplaats> begraafplaatsenArrayList;
    //private LayoutInflater inflater;

    public LijstBegraafplaatsenAdapter(Context context, ArrayList <Begraafplaats> begraafplaatsArray) {
        super(context,0 ,begraafplaatsArray);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //Get the data item for this position
        Begraafplaats begraafplaats = getItem(position);

        //Check if an existing view is being used, ontherwise inflate the view
        if (convertView == null){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.overledene_list_item,parent, false);
        }

        //lookup view for data population
        TextView parknummerTv = (TextView) convertView.findViewById(R.id.tv_parknummer_mapscreen);
        TextView begraafplaatsTV = (TextView) convertView.findViewById(R.id.tv_begraafplaats_mapscreen);

        // Populate the data into the template view using the data object
        parknummerTv.setText(begraafplaats.getPark()+ begraafplaats.getRow());
        begraafplaatsTV.setText((CharSequence) begraafplaats.getPark());

        //Return the completed view to reader on screen
        return convertView;
    }
}
