package com.example.doortje.begraafplaatsen.Models;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.doortje.begraafplaatsen.R;

import java.util.ArrayList;

import static com.example.doortje.begraafplaatsen.R.id.tv_sterftejaar_gevondenoverledene_searchscreen;

/**
 * Created by doortje on 3/07/15.
 * http://www.javacodegeeks.com/2013/09/android-listview-with-adapter-example.html
 */

//voor de aanmaak van een Adapter kan je kiezen voor een ArrayAdapter of een BaseAdapter.
// Omdat je met een lijst werkt maakt het niet uit welke je kiest. In de BaseAdapter moet je de
// methode specifiek importeren. Met een ArrayAdapter zit al heel veel ingebouwd.
public class LijstOverledenenAdapter extends ArrayAdapter<Overledene> {

    //private ArrayList <Overledene> overledeneArrayList;
    //private LayoutInflater inflater;

    public LijstOverledenenAdapter(Context context, ArrayList<Overledene> overledeneArray) {
        super(context, 0, overledeneArray);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        //Get the data item for this position
        Overledene overledene = getItem(position);

        //Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.overledene_list_item, parent, false);
        }

        //lookup view for data population
        TextView achternaamTV = (TextView) convertView.findViewById(R.id.tv_achternaam_gevondenoverledene_searchscreen);
        TextView voornaamTV = (TextView) convertView.findViewById(R.id.tv_voornaam_gevondenoverledene_searchscreen);
        TextView geboortejaarTV = (TextView) convertView.findViewById(R.id.tv_geboortejaar_gevondenoverledene_searchscreen);
        TextView sterftejaarTV = (TextView)convertView.findViewById(tv_sterftejaar_gevondenoverledene_searchscreen);

        //Populate the data into the template view using the data object
        achternaamTV.setText(overledene.getLast_name());
        voornaamTV.setText(overledene.getFirst_name());
        geboortejaarTV.setText((CharSequence) overledene.getDate_of_birth());
        sterftejaarTV.setText((CharSequence) overledene.getDate_of_death());

        //Return the completed view to reader on screen
        return convertView;


    }
}
