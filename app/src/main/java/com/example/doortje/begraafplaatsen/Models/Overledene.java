package com.example.doortje.begraafplaatsen.Models;

/**
 * Created by doortje on 3/07/15.
 */
public class Overledene {

    private String id;
    private String name;
    private String identification_number;
    private String last_name;
    private String first_name;
    private String place_of_birth;
    private String date_of_death;
    private String date_of_burial;
    private String date_of_birth;
    private String location_number;
    private String date_last_modification;
    private String nature_entombment;
    private String registration_number;
    private String burial_place_type;

    public Overledene() {
    }

    public Overledene(String id, String name, String identification_number,
                      String last_name, String first_name,
                      String date_of_death, String date_of_burial, String location_number,
                      String date_last_modification, String nature_entombment,
                      String registration_number, String burial_place_type) {
        this.id = id;
        this.name = name;
        this.identification_number = identification_number;
        this.last_name = last_name;
        this.first_name = first_name;
        this.date_of_death = date_of_death;
        this.date_of_burial = date_of_burial;
        this.location_number = location_number;
        this.date_last_modification = date_last_modification;
        this.nature_entombment = nature_entombment;
        this.registration_number = registration_number;
        this.burial_place_type = burial_place_type;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdentification_number() {
        return identification_number;
    }

    public void setIdentification_number(String identification_number) {
        this.identification_number = identification_number;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getPlace_of_birth() {
        return place_of_birth;
    }

    public void setPlace_of_birth(String place_of_birth) {
        this.place_of_birth = place_of_birth;
    }

    public String getDate_of_death() {
        return date_of_death;
    }

    public void setDate_of_death(String date_of_death) {
        this.date_of_death = date_of_death;
    }

    public String getDate_of_burial() {
        return date_of_burial;
    }

    public void setDate_of_burial(String date_of_burial) {
        this.date_of_burial = date_of_burial;
    }

    public String getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(String date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getLocation_number() {
        return location_number;
    }

    public void setLocation_number(String location_number) {
        this.location_number = location_number;
    }

    public String getDate_last_modification() {
        return date_last_modification;
    }

    public void setDate_last_modification(String date_last_modification) {
        this.date_last_modification = date_last_modification;
    }

    public String getNature_entombment() {
        return nature_entombment;
    }

    public void setNature_entombment(String nature_entombment) {
        this.nature_entombment = nature_entombment;
    }

    public String getRegistration_number() {
        return registration_number;
    }

    public void setRegistration_number(String registration_number) {
        this.registration_number = registration_number;
    }

    public String getBurial_place_type() {
        return burial_place_type;
    }

    public void setBurial_place_type(String burial_place_type) {
        this.burial_place_type = burial_place_type;
    }

    @Override
    public String toString() {
        return "Overledene{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", identification_number='" + identification_number + '\'' +
                ", last_name='" + last_name + '\'' +
                ", first_name='" + first_name + '\'' +
                ", place_of_birth='" + place_of_birth + '\'' +
                ", date_of_death='" + date_of_death + '\'' +
                ", date_of_burial='" + date_of_burial + '\'' +
                ", date_of_birth='" + date_of_birth + '\'' +
                ", location_number='" + location_number + '\'' +
                ", date_last_modification='" + date_last_modification + '\'' +
                ", nature_entombment='" + nature_entombment + '\'' +
                ", registration_number='" + registration_number + '\'' +
                ", burial_place_type='" + burial_place_type + '\'' +
                '}';
    }
}


